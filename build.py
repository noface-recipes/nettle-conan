#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conan.packager import ConanMultiPackager

def main():
    builder = ConanMultiPackager(
                    reference=get_package_reference(),
                    build_types=["Release"])

    builder.add_common_builds(shared_option_name="nettle:shared", pure_c=True)

    add_pic_builds(builder, pic_option_name    ="nettle:use_pic",
                            shared_option_name ="nettle:shared")

    builder.run()


#####################################################################

def add_pic_builds(builder, pic_option_name, shared_option_name):
    '''Add builds considering PIC(Position Independent Code) variations.
        For all shared builds, enables PIC.
        For static builds, includes both versions with and without PIC.
    '''

    builds = []

    for settings, options, env_vars, build_requires in builder.builds:
        options[pic_option_name] = True

        builds.append([settings, options, env_vars, build_requires])

        if options[shared_option_name] == False:
            copy_options = options.copy()
            copy_options[pic_option_name] = False

            builds.append([settings, copy_options, env_vars, build_requires])

    builder.builds = builds

def get_package_reference():
    pkg_info = get_package_info()

    return "{}/{}".format(pkg_info[0], pkg_info[1])

def get_package_info():
    import inspect
    import conanfile

    for name, member in inspect.getmembers(conanfile, is_recipe):
         if member.__module__ == conanfile.__name__:
             return (member.name, member.version)

    return None

def is_recipe(member):
    import inspect
    from conans import ConanFile

    return inspect.isclass(member) and issubclass(member, ConanFile)

if __name__ == "__main__":
    main()
