#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <nettle/sha1.h>

#define BUF_SIZE 1000

static void
display_hex(unsigned length, uint8_t *data)
{
  unsigned i;

  for (i = 0; i<length; i++)
    printf("%02x ", data[i]);

  printf("\n");
}

int
main(int argc, char **argv)
{
  struct sha1_ctx ctx;
  uint8_t digest[SHA1_DIGEST_SIZE];

  printf("*********************** Testing nettle *********************\n");

  sha1_init(&ctx);

  const char * message = "Hello World!";
  sha1_update(&ctx, strlen(message), message);

  sha1_digest(&ctx, SHA1_DIGEST_SIZE, digest);

  printf("sha1(\"%s\"):\n", message);
  display_hex(SHA1_DIGEST_SIZE, digest);

  printf("************************************************************\n");

  return EXIT_SUCCESS;  
}
