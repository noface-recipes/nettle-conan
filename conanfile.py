from conans import ConanFile
import os

from conans import tools
from conans.tools import download, unzip, check_md5

class NettleConan(ConanFile):
    name        = "nettle"
    version     = "3.3"
    description = "Nettle is a low-level cryptographic library"
    settings    =  "os", "compiler", "arch"

    url         = "https://gitlab.com/noface-recipes/nettle-conan"
    forked_from = "http://github.com/DEGoodmanWilson/conan-nettle"
    nettle_home = "https://www.lysator.liu.se/~nisse/nettle/"

    options         = {
        "shared"    : [True, False],
        "use_pic"   : ["default", True, False]
    }
    default_options = "shared=False", "use_pic=default"

    requires        = 'gmp/6.1.2@noface/stable'
    build_requires  = "AutotoolsHelper/0.1.0@noface/stable"

    ZIP_FOLDER_NAME = "nettle-%s" % version

    def source(self):
        zip_name = "nettle-%s.tar.gz" % self.version
        download("https://ftp.gnu.org/gnu/nettle//%s" % zip_name, zip_name)
        check_md5(zip_name, "10f969f78a463704ae73529978148dbe")
        unzip(zip_name)
        os.unlink(zip_name)

    def configure(self):
        del self.settings.compiler.libcxx

        if self.options.shared or self.options.use_pic==True:
            self.options.use_pic=True
            self.options["gmp"].use_pic = True

    def build(self):
        if tools.os_info.is_windows:
            self.output.warn("Build may not work on windows!")

        self.build_with_configure()
        
    def build_with_configure(self):
        with tools.chdir(self.ZIP_FOLDER_NAME), tools.pythonpath(self):
            from autotools_helper import Autotools

            autot = Autotools(self,
               shared      = self.options.shared)

            if self.options.use_pic != "default":
                autot.fpic = self.options.use_pic

            autot.configure()
            autot.build()
            autot.install()

    def package(self):
        self.output.info("Files already installed in build")
        
    def package_info(self):
        self.cpp_info.libs = ['nettle', 'hogweed']


